# ![logo](assets/images/logo-wordpress-plugins.png) {{PluginName}}

## Description

Add new modules to elementor.

-   Version: 1.0.0
-   Requires PHP: 7.1

## Installation

1. Move folder {{PluginName}} in folder wp-content > plugins
2. In your site's admin panel, go to Plugins > Installed Plugins.
3. Search "{{PluginName(-)}}" in the list of plugins.
4. Click Activate to start using le plugin.

```
git clone https://gitlab.com/David-_-/{{PluginName}}.git
```

## Development

-   roave/security-advisories
-   dealerdirect/phpcodesniffer-composer-installer
-   wp-coding-standards/wpcs
-   phpcompatibility/phpcompatibility-wp
-   friendsofphp/php-cs-fixer"
-   php-parallel-lint/php-console-highlighter"
-   phpro/grumphp
-   php-parallel-lint/php-parallel-lint
-   phpstan/phpstan
-   szepeviktor/phpstan-wordpress
-   sirbrillig/phpcs-variable-analysis

```
composer i
```
