<?php
/**
 * Plugin Name: PluginName
 * Description: Add new modules to elementor.
 * Plugin URI: https://gitlab.com/David-_-/PluginName
 * Author: David-_-
 * Version: 1.0.0.
 * Requires PHP 7.1
 *
 * @package PluginName
 *
 * Text Domain: PluginName
 */

declare( strict_types = 1 );

define( 'TEXTDOMAIN', 'PluginName' );

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

/**
* Fires once activated plugins have loaded.
*
* @source https://developer.wordpress.org/reference/hooks/plugins_loaded/
*
* Pluggable functions are also available at this point in the loading order.
* @return void
*/
add_action( 'plugins_loaded', 'plugin_init', 11 );

/**
 * Plugin init
 *
 * Initializing the plugin
 *
 * @return void
 */
function plugin_init() : void {

	load_plugin_textdomain( TEXTDOMAIN );
	require __DIR__ . '/src/app.php';
}
