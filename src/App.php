<?php
/**
 * App class
 *
 * @package PluginName
 */

declare( strict_types = 1 );

namespace PluginName;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

/**
 * App.
 *
 * The main App handler class is responsible for initializing.
 * The class registers and all the components required to run the applications.
 *
 * @version 0.0.1
 * @since 0.0.1
 */
class App {

	/**
	 * Instance.
	 *
	 * Holds the App instance.
	 *
	 * @since 0.0.1
	 * @access public
	 * @static
	 *
	 * @var App|null
	 */
	public static $instance = null;

	/**
	 * App.
	 *
	 * Ensures only one instance of the App class is loaded or can be loaded.
	 *
	 * @since 0.0.1
	 * @access public
	 * @static
	 *
	 * @return App instance of the class.
	 */
	public static function init() {

		if ( is_null( self::$instance ) ) {
			self::$instance = new self();

		}
		return self::$instance;
	}

	/**
	 * App constructor.
	 *
	 * Initializing applications.
	 *
	 * @since 0.0.1
	 * @access private
	 */
	private function __construct() {

		require dirname( __DIR__ ) . '/vendor/autoload.php';
	}
}
App::init();
